module.exports = {
    toMuch:"查看更多",
    navMenu: {
        'logo': "./../../static/img/logo.png",
        'title': ["首页", "云钱包", "HD钱包", "硬件钱包", "帮助中心", "GitHub", "EN"],
        'doucment': ["基础知识", "安全知识"],
    },
    index: {
        'screen1Title': "InWeCrypto 钱包",
        'screen1Title1': 'InWeCrypto 钱包是一款HD钱包的移动端数字资产钱包，支持BTC /ETH /NEO /ONT /EOS等多链，旨在为不同需求用户提供精准的解决方案。InWeCrypto 钱包安全性强、易于上手、功能丰富，让您恋上数字资产管理。',
        'screen1Btn': [
            {
                name: 'IOS',
                link: '../../static/img/QRIOS.png',
                icon: 'el-icon-seal-pingguo',
                txt: '下载需要使用非中国大陆的Apple ID登录App Store。'
            },
            {
                name: 'Android',
                link: '../../static/img/QRCodeEN.png',
                icon: 'el-icon-seal-anzhuo',
                txt: ''
            }
        ],
        'screen2Title': '多快好省',
        'screen2Content1': 'InWe钱包操作简单便捷，上手快；时刻关注业内动态，快速迭代更新，更好适应用户需求；用更短的时间在瞬息万变的加密货币世界把握转瞬即逝的机遇，精选不同方向领域DAPP，玩转区块链世界，当前已支持：ETH、NEO、ONT等链。',
        'screen2Content2': 'InWeCrypto 在手币种全有；资讯、交易所、dapp，多功能面面俱到。',
        'screen2advList':[
            {
                title:"多",
                content:"一站式管理多链、多地址、多资产及多钱包形态。",
                src:"./../../static/img/new/much.png",
            },
            {
                title:"快",
                content:"通过多重优化，将代币的转账速度打磨到了极速。",
                src:"./../../static/img/new/quick.png",
            },
            {
                title:"好",
                content:"去中心化账户和托管账户双核驱动，完美解决数字资产安全存储安全问题。",
                src:"./../../static/img/new/good.png",
            },
            {
                title:"省",
                content:"省心省力，用一套助记词管理所有链上资产，省却管理和记忆助记词的时间和风险成本。",
                src:"./../../static/img/new/save.png",
            }
        ],
        'advantageList':[
            {
                title:"多链管理  简单方便",
                content:"单组助记词管理多链资产，让多链资产管理更简单方便。当前已支持：ETH、NEO、ONT等链。",
                src:"./../../static/img/iphone7-plus.png",
            },
            {
                title:"多地址支持  更隐私",
                content:"HD（分层确定性）技术使单组助记词可支持多地址变换，用户可方便选择链上交易地址，使交易更隐私。",
                src:"./../../static/img/singleShadow.png",
            },
            {
                title:"多资产类型 更丰富",
                content:"全面支持基于ETH的ERC-20、ERC-721类型的token和基于NEO的NEP-5类型的token,交易类型更丰富。",
                src:"./../../static/img/iphone6-plus.png",
            },
            {
                title:"去中心化  更安全",
                content:"助记词或私钥由用户绝对控制，链上资产更安全。",
                src:"./../../static/img/triangle.png",
            }
        ],
        'screen4Title': {
            title:"云钱包",
            content:"InWeCrypto 云钱包专为入门级用户贴心打造，无需导入私钥和助记词，使用用户名密码就能轻松搞定，支持密码找回站内收发零费用，秒到账",
            src:"../../static/img/new/2-cloud.png"
        },
        'screen4Content': [
            {
                src:"../../static/img/new/kuaijie.png",
                title:"方便快捷",
                content:"打开即使用，无需导入私钥或助记词"
            },
            {
                src:"../../static/img/new/anquan.png",
                title:"更安全高效",
                content:"冷热钱包分离系统，更安全，资产管理更高效，资产充提更便捷"
            },
            {
                src:"../../static/img/new/tabbar_Quotation.png",
                title:"行情走势更清晰",
                content:"内置行情功能，让您在瞬息万变的世界中，及时获取行情走向"
            },
            {
                src:"../../static/img/new/jishuzhichi.png",
                title:"多重技术保障",
                content:"多重签名技术保障，验证码、指纹、活体等多种验证方式，保障账户安全"
            },
        ],
        'screen5Title': {
            title:"HD钱包",
            content:"InWeCrypto HD钱包是一款去中心化开源的分层确定性轻钱包，用户通过单组助记词即可安全、方便、简单的管理脸上数字资产，同时钱包内集成了不同类目区块链应用，让您轻松玩转DAPP",
            src:"../../static/img/new/1-hd.png"
        },
        'screen5Content': [
            {
                src:"../../static/img/new/quzhongxinhua-1.png",
                title:"去中心化",
                content:"私钥与助记词具有支持多链、多地址、多资产的特点，用户通过一套助记词即可安全、方便、简单的管理链上数字资产"
            },
            {
                src:"../../static/img/new/yinsi.png",
                title:"交易更隐私",
                content:"HD（分层确定性）技术使单组助记词可支持多地址变换，用户可方便选择链上交易地址，使交易更隐私"
            },
            {
                src:"../../static/img/new/shengtai-1.png",
                title:"多资产类型 更丰富",
                content:"全面支持基于ETH的ERC-20、ERC-721类型的Token和基于NEO的NEP-5类型的Token,交易类型更丰富"
            },
            {
                src:"../../static/img/new/shengtai.png",
                title:"开放第三方生态",
                content:"多样性DApp应用平台，开放第三方应用入驻，全方位的DApp，是您多元化的区块链世界入口"
            },
        ],
        'screen6Title': {
            title:"硬件钱包",
            content:"为大额资产用户及企业级用户量身定制",
            src:"../../static/img/new/3-hdw.png"
        },
        'screen6Content': [
            {
                src:"../../static/img/new/kuaijie.png",
                title:"安全可靠",
                content:"InWeCrypto 硬件钱包保证您加密资产的安全性，您的资产永远不会暴露。 通过加密算法对密钥进行防护和存储，为您的数字资产提供安全保障，多重防护只为更安全的资产管理"
            },
            {
                src:"../../static/img/new/anquan.png",
                title:"多种资产",
                content:"InWeCrypto 硬件钱包支持包括ETH、NEO、ONT等多链资产，管理各种类型的加密货币。同时InWeCrypto 硬件钱包允许您在同一硬件钱包中使用多地址进行资产的管理"
            },
            {
                src:"../../static/img/new/tabbar_Quotation.png",
                title:"简单易用",
                content:"InWeCrypto 硬件钱包支持多种使用场景，可搭配手机端和PC客户端，满足日常使用的需要。外观简约大气颇具质感"
            },
        ],
        'screen7Title':  {
            title:"支持资产种类",
            content:"InWeCrypto 钱包前瞻性的进行多资产布局，除支持erc20全局资产和nep5全局资产外，还特别支持erc721非同质化资产，为今后您独一无二的数字资产管理提供优质服务",
        },
        'screen7List1': [{
            name: 'ETH',
            icon: './../../static/img/new/ETH.png',
            link: '',
            },
            {
            name: 'USDT',
            icon: './../../static/img/new/USDT.png',
            link: '',
            },
            {
            name: 'WBA',
            icon: './../../static/img/new/WBA.png',
            link: '',
            },
            {
            name: 'WBT',
            icon: './../../static/img/new/WBT.png',
            link: '',
            },
            {
            name: 'NEO',
            icon: './../../static/img/new/NEO.png',
            link: '',
            },
            {
            name: 'ONT',
            icon: './../../static/img/new/ONT.png',
            link: '',
            },
        ],
        'screen8Title':  {
            title:"联系我们",
        },
        'screen8Input':{
            name:"名字",
            phone:"电话",
            email:"邮箱",
            content:"请输入您要编辑的..."
        },
        screen8Btn:"发送给我们",
        'screen7QRList': [{
            name: 'Android版本',
            QRcode: './../../static/img/1536114936783.jpg',
            link: ''
            },
            {
            name: 'iOS版本',
            QRcode: './../../static/img/1536114936783.jpg',
            link: ''
            }
        ],
        'screen7List': [{
            name: 'Telegram',
            icon: 'el-icon-seal-telegram',
            link: '',
            },
            {
            name: 'Twttier',
            icon: 'el-icon-seal-twitter',
            link: '',
            },
            {
            name: 'Reddit',
            icon: 'el-icon-seal-reddit',
            link: '',
            },
            {
            name: 'GutHub',
            icon: 'el-icon-seal-github',
            link: '',
            }
        ],
        emailTitle:"意见反馈",
        'imgList': ["./../../static/img/iphone7-plus.png", "./../../static/img/singleShadow.png", "./../../static/img/iphone6s-plus.png", "./../../static/img/triangle.png"]
    },
    footer:{
        email: [],
        logo:"../../static/img/logo-clr.png"
    },
    download:{
        title:"下载InWeCrypto 钱包",
        content:"InWeCrypto 钱包是一款集HD钱包、云钱包、硬件钱包为一体的综合性移动端数字资产钱包，支持BTC/ETH/NEO/ONT/EOS等多链，旨在为不同需求用户提供精准的解决方案。InWeCrypto 钱包安全性强、易于上手、功能丰富，让您恋上数字资产管理。",
        IOS:{
            hint: "注意: 一个终端不能同时安装企业签名版和海外版的APP",
            btn:[
                {
                    name:"企业签名版下载",
                    link:"../../static/img/ios/firmIOS.png",
                    url:"itms-services://?action=download-manifest&url=https://sealwallet.org/downloadApp/manifest.plist",
                },
                {
                    name:"海外Apple ID用户下载",
                    link:"../../static/img/ios/AppleIOS.png",
                    url:"https://itunes.apple.com/us/app/seal-wallet/id1426199859?mt=8"
                }
            ],
            explain:[
                {
                    txt:"什么是企业签名版 >>",
                    src:"downloadIOS",
                    type:"sign",
                },
                {   
                    txt:"什么是海外版 >>",
                    src:"downloadIOS",
                    type:"appStore",
                }
            ],
            errorMove:"您的手机是安卓系统,无法安装IOS应用",
            errorPC: "请在手机上使用浏览器打开本页面，或者扫描下面的二维码，即可安装",
        },
        Android:{
            hint: "注意: 一个终端不能同时安装企业签名版和海外版的APP",
            btn:[
                {
                    name:"企业签名版下载",
                    link:"../../static/img/QRCodeEN.png",
                },
                {
                    name:"Google Play用户下载",
                    link:"../../static/img/QRCodeEN.png",
                }
            ],
            explain:[
                // {
                //     txt:"什么安卓企业版 >>",
                //     src:"/downloadIOS"
                // },
                // {   
                //     txt:"什么Google Play版 >>",
                //     src:"/downloadIOS",
                // }
            ]
        },
    },
    faq:{
        title: "海报钱包",
        tabList:[{
                label:"基础知识",
                name:"basic"
            },
            {
                label:"安全知识",
                name:"security"
            },
            // {
            //     label:"使用指南",
            //     name:"guidance"
            // }
        ],
        kindList:{
            basic:["什么是数字钱包","什么是多链钱包","什么是私钥","什么是助记词","什么是矿工费"],
            security:["助记词泄露别人就能转移你的资产","助记词那么重要，我该如何保存","为什么需要 PIN 码"],
            // guidance:["如何下载","如何创建","如何导入","如何收款","如何付款","如何提取GAS/ONG","如何备份","如何资产管理","如何地址管理","如何多链管理"],
        },
        other:"其他文章",
        more:"查看更多",
        create:"创建于",
        loading:"加载中...",
        answer:{
            "什么是数字钱包": "<p>钱包 App 本质上是密钥的管理工具,它只保存密钥信息，里面不包含具体的代币资产。钱包中的密钥由成对的私钥和公钥组成，用户用使用私钥进行签名，证明资产所有权，确认交易信息，所有经过确认的交易信息都将存储在区块链中。</p>"+
            "<p>用户可以通过 Keystore, 助记词,私钥等方式导入钱包，其中 Keystore 是需要进行密码验证，而助记词和私钥可以直接导入，使用助记词和私钥时, 一定要注意周围环境，以确保资产安全。</p>",
            "什么是多链钱包":"在InWeCrypto 钱包中通过一个身份创建多链钱包，无需多个钱包来管理你的多个私钥或钱包地址，一套助记词便可管理不同的链上的资产以及不同种类的代币。到目前为止，InWeCrypto 钱包已经支持以太坊 ERC-20 代币、ERC-721 代币、和 NEP-5格式的代币。随着InWeCrypto 钱包的不断优化，将来将会支持更多种类的代币与数字资产。",
            "什么是私钥":"<p>私钥是用户加密资产的权益凭证。在区块链交易中, 私钥用于生成支付货币所必须的签名, 以证明资金的所有权。私钥必须始终妥善保管, 因为一旦泄露给第三方, 相当于对方直接获得该私钥下的全部资产。</p>"+
            "<p>私钥实际上并不是存储在网络中, 而是由用户生成并存储在一个文件或者简单的数据库中, 称为钱包。存储在用户钱包中的私钥完全独立, 可由用户的钱包软件生成并管理, 无需区块链或者网络连接。</p>",
            "什么是助记词":"助记词是私钥的另一种表现形式，其目的是为了帮助用户记忆复杂的私钥。助记词一般由 12、15、18、21 个单词构成，这些单词都取自一个固定词库，其生成顺序也是按照一定算法而来。任何人得到了你的助记词，可以不费吹灰之力的夺走你的资产。所以在用户在备份助记词之后，将备份后的助记词妥善保管，做好防盗防丢措施。",
            "什么是矿工费":"在区块链中向公有链中写数据时需要花费一定费用的, 这种开销有助于阻止垃圾内容, 并通过支付保护其安全性。网络上的任何节点(每个包含账本拷贝的连接设备被称作节点) 都可以参与称作挖矿的方式来保护网络。由于挖矿需要计算能力和电费, 所以矿工们的服务需要得到一定的报酬, 这也是矿工费的由来。",
            "助记词泄露别人就能转移你的资产":"助记词是私钥的另一种表现形式，其目的是为了帮助用户记忆复杂的私钥。而私钥是去中心化世界中资产凭证，因此当别人获取了你的助记词之后，不需要知道你的钱包地址与你设置的密码便可以转移你的资产。",
            "助记词那么重要，我该如何保存":"InWeCrypto 钱包建议采用物理介质备份，例如抄在纸上等方式，尽可能不要采用截屏或者拍照之后放在联网的设备里，以防被数字窃取。备份时请验证备份的助记词是否正确，一旦抄错单词，那么后续找回正确的助记词将会有巨大的困难。助记词备份后请妥善保管，做好防盗防丢措施。",
            "为什么需要 PIN 码":"在InWeCrypto 钱包的操作当中，除了创建身份和导入钱包时出现的助记词，在转账或者是备份钱包时，为了保证安全性，需要用户输入 PIN 码来验证，以保证数字钱包的资产安全。",
            // iframe:{
            //     "如何下载":"../../static/file/zh/下载安装.pdf.html",
            //     "如何创建":"../../static/file/zh/创建教程.pdf.html",
            //     "如何导入":"../../static/file/zh/导入教程.pdf.html",
            //     "如何收款":"../../static/file/zh/收款教程.pdf.html",
            //     "如何付款":"../../static/file/zh/转账教程.pdf.html",
            //     "如何提取GAS/ONG":"../../static/file/zh/提取GASNG教程.pdf.html",
            //     "如何备份":"../../static/file/zh/备份.pdf.html",
            //     "如何资产管理":"../../static/file/zh/资产管理教程.pdf.html",
            //     "如何地址管理":"../../static/file/zh/地址管理教程.pdf.html",
            //     "如何多链管理":"../../static/file/zh/多链管理教程.pdf.html",
            // }
        }
    },
    ios:{
        btn:"点击安装",
        sign:{
            title: "什么是企业签名版?",
            content:[
                    "1. 由于政策法规影响，数字货币钱包应用无法在中国大陆区App Store 上架",
                    "2. InWeCrypto 钱包国内版使用了苹果开发者账号的企业签名来分发APP，用户需要手动在【设置】中信任企业证书使用",
                    "3. 推荐有海外Apple ID的用户下载海外版"
                ],
            operation: {
                title:"无法打开企业签名版App怎么办？",
                content:[
                    {
                        title: "1.官网点击下载【企业签名版】，打开会提示【未受信任的签名证书】",
                        src: "",
                    },
                    {
                        title: "2.点击【设置】——选择【通用】——页面下方选择【设备管理】——找到签名证书【Chongqing Junhan Technology Co., Ltd】，点击信任即可打开App",
                        src: [
                            "../../../static/img/ios/sign/0.jpg","../../../static/img/ios/sign/1.png","../../../static/img/ios/sign/2.png","../../../static/img/ios/sign/3.png","../../../static/img/ios/sign/4.png","../../../static/img/ios/sign/5.png",
                        ],
                    }
                ]
            },
        },
        appStore:{
            title: "什么是海外版?",
            content:[
                    "1. 由于政策法规影响，数字货币钱包应用无法在中国大陆区App Store 上架",
                    "2. InWeCrypto 钱包已经在全球154个国家和地区上架App Store，海外版App和国内企业签名版的功能完全一致",
                    "3. 推荐有海外Apple ID的用户下载海外版"
                ],
            operation: {
                title:"使用海外Apple ID安装应用方法",
                content:{
                    title: "1.使用海外区Apple ID登录App Store（如果没有海外Apple ID请查看 <a href=\"#getAppleId\" style=\"color:#0983f5 \">如何获取海外Apple ID</a>）",
                    srcList: [
                        {
                            src:"../../../static/img/ios/store/1.png",
                            txt:"App Store首页右上角点击头像图标"
                        },
                        {
                            src:"../../../static/img/ios/store/2.png",
                            txt:"退出当前已登录的Apple ID"
                        },
                        {
                            src:"../../../static/img/ios/store/3.png",
                            txt:"账户页登录海外Apple ID"
                        },
                    ],
                    title1 :"2.登录成功后到搜索页面搜索【Seal wallet】，点击获取按钮即可安装。",
                },
            },
            ID:{
                title: "如何获取海外Apple ID？",
                content0:[
                    "方法一：某宝搜索【Apple海外/Apple 国外/国外 Apple】等关键词，购买海外版Apple ID后登录下载", 
                    "方法二：按照下方流程自行注册海外Apple ID"
                ],
                content1:{
                    title:"准备工作",
                    txt:"安装科学上网工具（推荐使用美国区），请确保自己在科学上网的环境（一定要在全局代理模式下）"
                },
                getContent:{
                    title:"注册流程",
                   content:[
                        {
                            title:"1.打开官网（https://appleid.apple.com），下拉到底部，将区域切换为United States。",
                            src:["../../../static/img/ios/store/4.png"]
                        },
                        {
                            title:"2.待界面全部切换为英文后，点击页面右上角的【Create Your Apple ID】",
                            src:["../../../static/img/ios/store/5.png"]
                        },
                        {
                            title:"3.这里需要使用一个未使用过的邮箱注册账号，按照要求填写信息即可。",
                            src:["../../../static/img/ios/store/6.png"]
                        },
                        {
                            title:"<p>4.注册成功会向你的邮箱发送一封确认邮件，将邮件中的验证码填写好就可以了。填写验证码后会跳转到一个展示你账号详细信息的界面，说明已经注册成功了。</p>"+
                            "<p>在此期间，有一定概率会出现【由于一些原因，当前帐号无法注册】这一提示，这时页面会跳转到原先的信息填写界面。不要慌，重新在底部填写验证，让苹果再给你发一封验证邮件，多试几次就会成功了。</p>",
                            src:""
                        },
                        {
                            title:"<p>5.创建好账户后就需要使用创建的Apple ID 登录App Store验证支付信息。这也是整个流程最关键的步骤。请确保自己是在科学上网的环境（一定要在全局代理模式下）</p><p>打开Apple Store登录个人账户，输入账户名和密码，点击【Sign In】</p>",
                            src:["../../../static/img/ios/store/7.png","../../../static/img/ios/store/8.png"],
                            merge:true,
                        },
                        {
                            title:"6.第一次登录会有提示，选择“Review”，界面跳转后将地区修改为“United States”，打开同意条款。",
                            src:["../../../static/img/ios/store/9.png","../../../static/img/ios/store/10.png"],
                            merge:true,
                        },
                        {
                            title:"7.点击“Next”，跳转到下一界面，填写个人信息。首先补全个人详细信息，点击“Next”跳转，然后补全付款信息，在这里如果操作正确，会出现“None”的选项（这个选项只有处于全局代理的网络坏境下才会出现），点击勾选。然后填写账单地址，这里可以通过浏览器搜索“美国地址生成器”。",
                            src:["../../../static/img/ios/store/11.png","../../../static/img/ios/store/12.png"],
                            merge:true,
                        },
                        {
                            title:"8.填写好后，你的美国Apple ID就注册成功啦，搜索“InWeCrypto 钱包”下载使用吧",
                            src:""
                        },
                    ]
                },

            }
        },
        hint:{
            title:"点击右上角选择浏览器打开 ~ ~",
            content:"请在 safari浏览器打开"
        }
    }
}