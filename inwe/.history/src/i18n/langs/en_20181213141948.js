module.exports =  {
    toMuch:"More",
    navMenu: {
        'logo': "./../../static/img/logo.png",
        'title': ["Index", "Cloud wallet", "HD wallet",  "Hardware wallet", "Help center", "GitHub", "中文"],
        'doucment': ["Basic Knowledges", "Security Knowledges", "User Guidance"],
        // 'doucment': ["Basic Knowledges", "Security Knowledges", "User Guidance"],
    },
    index: {
        screen1Title: "Seal Wallet</br>Digital asset management expert",
        screen1Title1: 'Seal wallet is a comprehensive digital asset wallet app that combines HD wallet, Cloud wallet and hardware wallet. It supports BTC, ETH, NEO, ONT and EOS, etc., aims to provide precise solutions for users who have different needs. Seal wallet has extreme stability, reliability and safety, simple operation allows you to enjoy digital asset management.',
        'screen1Btn': [{
                name: 'IOS',
                link: '../../static/img/QRIOS.png',
                icon: 'el-icon-seal-pingguo',
                txt: 'Non-China Apple ID is required to download.'
            },
            {
                name: 'Android',
                link: '../../static/img/QRCodeEN.png',
                icon: 'el-icon-seal-anzhuo',
                txt: ''
            }
        ],
        'screen2Title': ' Multiple Fast Excellent Convenient',
        'screen2Content1': 'Seal wallet is simple to operate, focus on the industry dynamics, high-speed update is better to adapt user needs. In this ever-changing cryptocurrency world, Seal wallet grasp the opportunities, selects different kinds of DAPP, let you enjoy the blockchain world.',
        'screen2Content2': 'Currently the wallet has supported: ETH, NEO, ONT chain.',
        'screen2advList':[
            {
                title:"Multiple",
                content:"Seal wallet supports multi-chain, multi-address and multi-assets.",
                src:"./../../static/img/new/much.png",
            },
            {
                title:"Fast",
                content:"Through multiple optimizations, the transfer rate of tokens is speeded to the highest speed.",
                src:"./../../static/img/new/quick.png",
            },
            {
                title:"Excellent",
                content:"Have both decentralized account and escrow account, excellently solves the security problem of digital asset storage.",
                src:"./../../static/img/new/good.png",
            },
            {
                title:"Convenient",
                content:"Manage multi-chain assets by only a set of mnemonics, eliminate the time and risk costs of managing and remembering mnemonics.",
                src:"./../../static/img/new/save.png",
            }
        ],
        'screen4Title': {
            title:" Cloud wallet",
            content:"Seal cloud wallet is designed for entry-level users，without the need to import private key or mnemonics, just use username and password to successfully get it. Cloud wallet supports password recovery, transfer assets for free with lightning speed.",
            src:"../../static/img/new/2-cloud.png"
        },
        'screen4Content': [
            {
                src:"../../static/img/new/kuaijie.png",
                title:"Convenient",
                content:"Don’t need to import private key or mnemonics, just open the App, then you can enjoy your digital asset trip."
            },
            {
                src:"../../static/img/new/anquan.png",
                title:"Safer and more efficient",
                content:"Separate the cold wallet and hot wallet, makes the digital assets safer and more efficient."
            },
            {
                src:"../../static/img/new/tabbar_Quotation.png",
                title:"Get the latest market trends",
                content:"Built-in market analysis function could let you get the latest information of the market."
            },
            {
                src:"../../static/img/new/jishuzhichi.png",
                title:"Multiple technical support",
                content:"Multiple signature technology, verification code and fingerprint verification enhance the account protection."
            },
        ],
        'screen5Title': {
            title:"HD wallet",
            content:"Seal HD wallet is a decentralized open source hierarchical deterministic wallet, users could manage on-chain digital assets by only a set of mnemonics, making multi-chain asset management easier and more convenient. Seal HD wallet also integrates different blockchain applications, you can experience DAPP here!",
            src:"../../static/img/new/1-hd.png"
        },
        'screen5Content': [
            {
                src:"../../static/img/new/quzhongxinhua-1.png",
                title:"Decentralized",
                content:"Private key and mnemonics have the characters of supporting multi-chain, multi-address and multi-assets, users just need to use a set of mnemonics to manage their on-chain digital asset safely, conveniently and simply."
            },
            {
                src:"../../static/img/new/yinsi.png",
                title:"More private",
                content:"Hierarchical deterministic technology enables a single set of mnemonics to support multi-address transaction, users could select the on-chain transaction address, makes the transaction more private."
            },
            {
                src:"../../static/img/new/shengtai-1.png",
                title:"multi-asset types  much richer",
                content:"Fully support for ETH-based ERC-20，ERC-721 tokens and NEO-based NEP-5 tokens, with much richer transaction types."
            },
            {
                src:"../../static/img/new/shengtai.png",
                title:"open up the third-party ecology",
                content:"Seal wallet provides a multifarious DApp platform, supports the third-party App to enter and build the ecology together. Full-scale DApp is the entrance of the blockchain world."
            },
        ],
        'screen6Title': {
            title:" Hardware wallet",
            content:"Tailored to the users with large amount of assets or enterprises users",
            src:"../../static/img/new/3-hdw.png"
        },
        'screen6Content': [
            {
                src:"../../static/img/new/kuaijie.png",
                title:"Safe and reliable",
                content:"The Seal hardware wallet guarantees the security of your encrypted assets and your assets will never be exposed. Encrypt and store keys with encryption algorithms could enhance the protection of your digital assets, makes asset management more secure."
            },
            {
                src:"../../static/img/new/anquan.png",
                title:"Multiple asset types",
                content:"Seal hardware wallet supports ETH, NEO, ONT, and many of other chain assets, users could manage different types of cryptocurrency here. It’s also allows users to manage assets by different address in the same hardware wallet."
            },
            {
                src:"../../static/img/new/tabbar_Quotation.png",
                title:"Simple to operate",
                content:"Seal hardware wallet supports a variety of usage scenarios, and can be used with mobile phones and PC clients to meet the daily needs."
            },
        ],
        'screen7Title':  {
            title:"supportable asset types",
            content:"Seal hardware wallet not only supports ERC-20 and NEP-5 assets, but specifically supports ERC-721 non-homogeneous assets, providing quality service for your unique digital asset management.",
        },
        'screen7List1': [{
            name: 'ETH',
            icon: './../../static/img/new/ETH.png',
            link: '',
            },
            {
            name: 'USDT',
            icon: './../../static/img/new/USDT.png',
            link: '',
            },
            {
            name: 'WBA',
            icon: './../../static/img/new/WBA.png',
            link: '',
            },
            {
            name: 'WBT',
            icon: './../../static/img/new/WBT.png',
            link: '',
            },
            {
            name: 'NEO',
            icon: './../../static/img/new/NEO.png',
            link: '',
            },
            {
            name: 'ONT',
            icon: './../../static/img/new/ONT.png',
            link: '',
            },
        ],
        'screen8Title':  {
            title:"Contact us",
        },
        'screen8Input':{
            name:"Name",
            phone:"Telephone",
            email:"E-mail",
            content:"Please enter the..."
        },
        emailTitle:"Feedback",
        screen8Btn:"Send it to us",
        'screen7QRList': [{
            name: 'Android',
            QRcode: './../../static/img/1536114936783.jpg',
            link: ''
            },
            {
            name: 'iOS',
            QRcode: './../../static/img/1536114936783.jpg',
            link: ''
            }
        ],
        'screen7List': [{
            name: 'Telegram',
            icon: 'el-icon-seal-telegram',
            link: '',
            },
            {
            name: 'Twttier',
            icon: 'el-icon-seal-twitter',
            link: '',
            },
            {
            name: 'Reddit',
            icon: 'el-icon-seal-reddit',
            link: '',
            },
            {
            name: 'GutHub',
            icon: 'el-icon-seal-github',
            link: '',
            }
        ],
        'screen7Email': [" Contact us ", "Technical support", "sealwallet@gmail.com"],
        'imgList': ["./../../static/img/21.png", "./../../static/img/22.png", "./../../static/img/23.png", "./../../static/img/triangle1.png"]
    },
    footer:{
        email: ["Contact us", "If you have some good ideas, please send them to us!", "E-mail: sealwallet@gmail.com"],
        logo:"../../static/img/logo-clr.png"
    },
    download:{
        title:"Download seal wallet",
        content:"Seal wallet is a comprehensive digital asset wallet app that combines HD wallet, Cloud wallet and hardware wallet. It supports BTC, ETH, NEO, ONT and EOS, etc., aims to provide precise solutions for users who have different needs. Seal wallet has extreme stability, reliability and safety, simple operation allows you to enjoy digital asset management.",
        IOS:{
            hint:"Note: one terminal cannot install both enterprise signature edition and overseas edition of the APP",
            btn:[
                {
                    name:"Enterprise sign edition",
                    link:"../../static/img/ios/firmIOS.png",
                    url:"itms-services://?action=download-manifest&url=https://sealwallet.org/downloadApp/manifest.plist",
                },{
                    name:"Overseas edition",
                    link:"../../static/img/ios/AppleIOS.png",
                    url:"https://itunes.apple.com/us/app/seal-wallet/id1426199859?mt=8"
                }
            ],
            explain:[
                {
                    txt:"What is enterprise signature edition >>",
                    src:"downloadIOS",
                    type:"sign",
                },
                {   
                    txt:"What is the Overseas Edition >>",
                    src:"downloadIOS",
                    type:"appStore",
                }
            ],
            errorMove:"Your cell phone is Adnroid，cannot install iOS app",
            errorPC:"For install app, please use the mobile-browser to open this page, or scan the QR code below.",
        },
        Android:{
            hint: "注意: 一个终端不能同时安装企业签名版和海外版的APP",
            btn:[
                {
                    name:"企业签名版下载",
                    link:"../../static/img/QRCodeEN.png",
                    url:"",
                },
                {
                    name:"Google Play用户下载",
                    link:"../../static/img/QRCodeEN.png",
                    url:"",
                }
            ],
            // explain:[
            //     {
            //         txt:"什么安卓企业版 >>",
            //         src:"downloadIOS",
            //         type:"sign",
            //     },
            //     {   
            //         txt:"什么Google Play版 >>",
            //         src:"downloadIOS",
            //         type:"appStore",
            //     }
            // ]
        },
    },
    faq:{
        title: "seal Wallet",
        tabList:[{
                label:"Basic Knowledges",
                name:"basic"
            },
            {
                label:"Security Knowledges",
                name:"security"
            },
            // {
            //     label:"User Guidance",
            //     name:"guidance"
            // }
        ],
        kindList:{
            basic:["What is Digital Wallet","What is Multi-chain Wallet","What is Private Key","What is Mnemonic Phrase","What is Mining Fee"],
            security:["Why the Private Key is Important","Why the PIN Code is important","How to Store Private Key",],
            // guidance:[
            // "How to Download Wallet","How to Create Wallet","How to Import Wallet", "How to Receive","How to Transfer","How to Claim GAS/ONG","How to Backup Wallet","How to Manage Assets","How to Manage Address","How to Manage Chains"
            // ],
        },
        other:"Other articles",
        more:"More",
        create:"create",
        loading:"loading...",
        answer:{
            "What is Digital Wallet" : "<p>Essentially, wallet App is a kind of management tool of the private key, it doesn’t contain any specific token assets. And there is a secret key in wallet, which is composed of private key and public key in pairs. Private key enables the user to make a digital signature to prove the ownership of assets, and confirm the transaction information. All the confirmed transaction information will be stored in blockchain.</p>"+"<p>We often use Keystore, mnemonics or private key to import wallet, When using Keystore to import, password verification is necessary. Mnemonics and private key don’t need any verification but you must keep them well, be sure anybody cannot find them, or your assets will at great risk.</p>",
            "What is Multi-chain Wallet" : "<p>In Seal wallet, you just need to create one wallet to manage different chains and assets with one mnemonic. You don‘t need to download lots of wallets in different chains to keep your assets anymore. At present, you can manage ETH, NEO and ONT in wallet. Of course, not only stop there, Seal wallet will support more different chains and assets in the future.</p>",
            "What is Private Key" : "<p>It is the secret half of your address and is a string of 64 hexadecimal characters. If you write down your private key differently today than yesterday, you will be accessing a different wallet. Never write your private key by hand.</p>"+"<p>A private key typically looks like this:</p>" + "<strong  style='word-wrap:break-word'>afdfd9c3d2095ef696594f6cedcae59e72dcd697e2a7521b1578140422a4f890.</strong>" + "<p>Without your private key, you cannot access your funds. To keep it secure and easier to retrieve and remember, you can save the private key using a keystore (file or string) or mnemonic phrase. </p>",
            "What is Mnemonic Phrase" : "<p>A mnemonic phrase is a human readable encrypted private key. It is usually used to derive multiple private keys. It is a 12/15/18/21/24 words phrase that can help you access an infinite amount of accounts.<p>"+"<p>Mnemonic phrases are used by Seal wallet and many other wallets. Each of these examples is \"paths\" and the account you access is linked to the mnemonic phrase (its encrypted private key). If you select a different path it will generate a different address.Here is an example of what it should look like: </p>"+"<strong>scissors invite lock maple supreme raw rapid void congress muscle digital elegant little brisk hair mango congress clump</strong>",
            "What is Mining Fee" : "<p>The amount remaining when the value of all outputs in a transaction are subtracted from all inputs in a transaction is the mining fee. The fee is paid to the miner who include that transaction in a block.</p>"+"<p>In bitcoin this is based solely on transaction size, in Ethereum the fees are paid in gas and calculated based on contract code execution complexity.</p>",
            "Why the Private Key is Important":"A Mnemonic Phase is another form of the private key which is intended to help the users remember the Private Key with it rather than directly the complex Private Key. The Private Key is the certificate of assets in the decentralized world, so when someone else gets your Mnemonic Phrase, and your assets could be taken away form your wallet.",
            "Why the PIN Code is important":"Seal wallet recommends to use physical media to backup, such as copying on paper, please do not to take screenshots or take pictures and put them on networked devices to prevent digital theft. When verifying, please verify that the Mnemonic Phrase is correct. Once you have copied the wrong word, it will be very difficult to retrieve the correct one. After the backup of the Mnemonic, please keep it safely and take measures to prevent theft",
            "How to Store Private Key":"In the operation of the seal wallet, in addition to the Mnemonic Phrase that appear when creating a wallet and importing the wallet, in order to ensure security when transferring token or backing up the wallet, the user needs to input a PIN Code to verify the identity",
            // iframe:{
            //     "How to Download Wallet":"../../static/file/en/HowoownloadealDallet.pdf.html",
            //     "How to Create Wallet":"../../static/file/en/HoworeateealDallet.pdf.html",
            //     "How to Import Wallet":"../../static/file/en/HowomportealDallet.pdf.html",
            //     "How to Receive":"../../static/file/en/HowoeceiveithealDallet.pdf.html",
            //     "How to Transfer":"../../static/file/en/HoworansferithealDallet.pdf.html",
            //     "How to Claim GAS/ONG":"../../static/file/en/HowolaimASrNG.pdf.html",
            //     "How to Backup Wallet":"../../static/file/en/HowoackupealDallet.pdf.html",
            //     "How to Manage Assets":"../../static/file/en/Howoanagessets.pdf.html",
            //     "How to Manage Address":"../../static/file/en/Howoanageddresses.pdf.html",
            //     "How to Manage Chains":"../../static/file/en/Howoanagehains.pdf.html",
            // }
        }
    },
    ios:{
        btn:"install",
        sign:{
            title: "What is enterprise signature edition?",
            content:[
                    "1. Due to the influence of policies and regulations, the digital currency wallet application cannot be released in the App Store in mainland China.",
                    "2. The domestic edition of Seal wallet uses the corporate signature of the Apple Developer account to distribute the App. You need to manually trust the Enterprise Certificate in[Settings]",
                    "3. If you have overseas Apple ID, we recommend to download the overseas edition."
                ],
            operation: {
                title:"What should I do when the enterprise signature edition cannot be opened?",
                content:[
                    {
                        title: "1. Enter the Seal wallet official website and download the enterprise signature edition, when open the App, it will prompt “Untrusted Enterprise Developer”.",
                        src: "",
                    },
                    {
                        title: "2. Tap [Settings] — select[General] — select [Device Management] — find the enterprise developer[Chongqing Junhan Technology Co., Ltd]，tap “Trust” then you can open the App.",
                        src: [
                            "../../../static/img/ios/sign/e0.jpg","../../../static/img/ios/sign/e1.png","../../../static/img/ios/sign/e2.png","../../../static/img/ios/sign/e3.png","../../../static/img/ios/sign/e4.png","../../../static/img/ios/sign/e5.png",
                        ],
                    }
                ]
            },
        },
        appStore:{
            title: "What is Overseas Edition?",
            content:[
                    "1. Due to the influence of policies and regulations, the digital currency wallet application cannot be released in the App Store in mainland China.",
                    "2. Seal Wallet has been released in the App Store in 154 countries and regions around the world. The overseas edition is totally identical to the enterprise signature edition of the mainland China.",
                    "3. If you have overseas Apple ID, we recommend to download the overseas edition."
                ],
            operation: {
                title:"How to Download Seal Wallet App with Overseas Apple ID?",
                content:{
                    title: "1. Please use the overseas Apple ID to sign in to App Store (if you don't have an overseas Apple ID, please read  <a href=\"#getAppleId\" style=\"color:#0983f5 \"> How to Get the Overseas Apple ID</a>)",
                    srcList: [
                        {
                            src:"../../../static/img/ios/store/1.png",
                            txt:"Tap the image in the top right corner of the home page"
                        },
                        {
                            src:"../../../static/img/ios/store/2.png",
                            txt:"Sign out the current Apple ID"
                        },
                        {
                            src:"../../../static/img/ios/store/3.png",
                            txt:"Sign in with the overseas Apple ID"
                        },
                    ],
                    title1 :"2. After signing in your Apple ID, search for [Seal wallet] and tap “Get” to install the App.",
                },
            },
            ID:{
                title: "How to Download Seal Wallet App with Overseas Apple ID?",
                content0:[
                    "Method 1: Search keywords such as “Apple overseas”, ”Apple Foreign ”,etc. in some online shopping platform, purchase an overseas Apple ID then download the App", 
                    "Method 2: Please follow the process below to register your overseas Apple ID."
                ],
                content1:{
                    title:"Preparation",
                    txt:"Install a legal VPN (we recommended to use the US area), please make sure that you are in a science network environment (must be in the global proxy pattern)"
                },
                getContent:{
                    title:"Registration process",
                   content:[
                        {
                            title:"1. Enter the Apple official website, pull it down to the button and switch the area to the United States",
                            src:["../../../static/img/ios/store/4.png"]
                        },
                        {
                            title:"2. When the page are switched to English, please tap [Create Your Apple ID] in the upper right corner of the page.",
                            src:["../../../static/img/ios/store/5.png"]
                        },
                        {
                            title:"3. Here you need to use an unused email to register your ID and fill in the information as required.",
                            src:["../../../static/img/ios/store/6.png"]
                        },
                        {
                            title:"<p>4. If the registration completes successfully, you will receive a confirmation email, just fill out the verification code in the email, then the it will jump to an interface showing the details of your Apple ID, indicating that the registration has been successful.</p>"+
                            "<p>During this period, maybe you will be prompted that “For some reasons, the current Apple ID cannot be registered”, then the page will jump to the original information filling interface. Don’t worry, just re-fill the verification at the bottom of the page, wait for the verification email again.</p>",
                            src:""
                        },
                        {
                            title:"<p>Once you have created your Apple ID, you will need to sign in to the App Store to verify your payment information. This is also the most critical step in the entire process. Make sure you are in a science network environment (must be in global proxy pattern).</p><p>Open the Apple Store and enter your Apple ID and Password, then tap [Sign In].</p>",
                            src:["../../../static/img/ios/store/7.png","../../../static/img/ios/store/8.png"],
                            merge:true,
                        },
                        {
                            title:"6.The first sign in will give you a prompt, select \"Review\", the interface will be switched to \"United States\", open the \"Agree to Terms and Conditions\". ",
                            src:["../../../static/img/ios/store/9.png","../../../static/img/ios/store/10.png"],
                            merge:true,
                        },
                        {
                            title:"7. Tap \"Next\" to jump to the next interface and fill in your information. Please fill in the personal information, tap \"Next\" , and then complete the payment method. If you did all the process correctly, there will be a \"None\" option (this option will only appear in Global Proxy network environment ), tap to check. Then fill in the billing address that you can search for \"US Address Generator\" in the internet.",
                            src:["../../../static/img/ios/store/11.png","../../../static/img/ios/store/12.png"],
                            merge:true,
                        },
                        {
                            title:"8. When you have completed all the above steps, your US Apple ID registration is succeed. Then you can search for \"Seal Wallet\" in App Store, download and use it.",
                            src:""
                        },
                    ]
                },

            }
        },
        hint:{
            title:"Tap the menu above and open the page in safari",
            content:"Please open it in the safari"
        }
    }
}
