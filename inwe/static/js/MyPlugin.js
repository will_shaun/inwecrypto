let MyPlugin = {};
MyPlugin.install = function (Vue, options) {

    // <div v-tooltip="'白昼行僵 暗夜魔王'"></div> 
    Vue.directive("tooltip",{
        bind:function(el, binding, vnode){
            let div = document.createElement("div");
            div.classList.add("tip-container");
            el.classList.add("tooltip");
            div.innerHTML = binding.value;
            el.appendChild(div);
            
        }
    })

    // demo :  <div class="box" v-overflowTip="{message:'与其感慨路难行,不然趁早出发',placement:'bottom'}"></div>
    Vue.directive('overflowTip',{
        inserted:function(el, binding, vnode){
            let div = document.createElement("div");
            let parent = el.parentNode;
            let txt = binding.value.message;
            let placement = binding.value.placement;
            if( "top" === placement){
                div.classList.add("tip-top");
            }else if( "bottom" === placement){
                div.classList.add("tip-bottom");
            }
            el.classList.add("overflowTip");
            div.classList.add("tip-container");

            div.innerHTML = txt ;
            el.innerText = txt;
            parent.appendChild(div);
            parent.classList.add("relative");
            parent.addEventListener("mouseleave", function (event) {
                if(div.style.display != "none"){
                    // setTimeout(()=>{
                        div.style.display = "none"
                    // },100)
                }
            })
            parent.addEventListener("mouseenter", function (event) {
                if(div.style.display === "none" || div.style.display === ""){
                    div.style.display = "block"
                }
            })
        }
    })

    // <p> {{ "don't worry be happy" |capitalize}}</p>
    Vue.filter('capitalize', function (value) {
        if (!value) return '';
        return value.charAt(0).toUpperCase() + value.slice(1);
        
    })

    // <p>{{1589923213014| dateFormat('yyyy-MM-dd hh:mm:ss')}}</p>
    /**
     * 把时间转化为时间戳
     * let date = new Date('2018-7-7 19:42:09'); 
     * date.getTime(); 
     * date.valueOf();
     * Date.parse("2018-10-10 19:42:09");
     */
    Vue.filter("dateFormat", function (value,format) {
        if (!value) return '';
        let getDate = new Date(value);
        let fmt;
        let obj = {
            'M+': getDate.getMonth() + 1,
            'd+': getDate.getDate(),
            'h+': getDate.getHours(),
            'm+': getDate.getMinutes(),
            's+': getDate.getSeconds(),
            'q+': Math.floor((getDate.getMonth() + 3) / 3),
            'S': getDate.getMilliseconds()
        };
        if (/(y+)/.test(format)) {
            // RegExp.$1...$9属性的值为String类型，返回上一次正则表达式匹配中，第n个子表达式所匹配的文本。此属性只保存最前面的9个匹配文本。
            fmt = format.replace(RegExp.$1, (getDate.getFullYear() + '').substr(4 - RegExp.$1.length))
        }
        for (let k in obj) {
            if (new RegExp('(' + k + ')').test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (obj[k]) : (('00' + obj[k]).substr(('' + obj[k]).length)))
            }
        }
          return fmt;
    })

    Vue.filter("", function(value){
        if (!value) return '';
    })
}
export default MyPlugin;