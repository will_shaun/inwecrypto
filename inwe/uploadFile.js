const qiniu = require("qiniu");
const fs = require('fs');

// 上传文件到七牛云
const uploadFile = async function ( file, uploadObj) {
    console.log("start");
    
    let obj = file;
    let key = obj.filename;
    let localFile = obj.path + obj.filename ;
    let mac = new qiniu.auth.digest.Mac( uploadObj.accessKey, uploadObj.secretKey);
    let putPolicy = new qiniu.rs.PutPolicy({"scope": uploadObj.bucket});
    let uploadToken =  putPolicy.uploadToken(mac);
    let config = new qiniu.conf.Config();
    // 空间对应的机房
    config.zone = uploadObj.zone;
    let formUploader = new qiniu.form_up.FormUploader(config);
    let putExtra = new qiniu.form_up.PutExtra();
    await formUploader.putFile(uploadToken, key, localFile, putExtra, function(respErr,
        respBody, respInfo) {
        if (respErr) {
            console.log("ERROR")
            throw respErr;
        }
        if (respInfo.statusCode == 200) {
            console.log("upload  " + obj.filename + " is success" );
            console.log(respBody);
        } else {
            console.log(respInfo.statusCode);
            console.log(respBody);
        }
    });
}
// 获取文件的全名
function readFileList(path, filesName) {
    var files = fs.readdirSync(path);
    var obj = {};
    files.forEach(function (itm, index) {
        var stat = fs.statSync(path + itm);
        if (stat.isDirectory()) {
        //递归读取文件
            readFileList(path + itm + "/", [])
        } else {
            if(itm.indexOf(filesName) > -1 &&  itm.indexOf("map") == -1){
            obj.path = path;//路径
            obj.filename = itm//名字
            }
        }
    })
    return obj;
}
module.exports = uploadFile;