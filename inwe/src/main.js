// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { Menu, MenuItem, Submenu, Card, Row, Col, Popover, MenuItemGroup,Tabs, TabPane, Loading,Input,Tooltip} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import MyPlugin from '../static/js/MyPlugin'
import './assets/icon/iconfont.css'
import VueI18n from 'vue-i18n';

Vue.use(Menu);
Vue.use(MenuItem);
Vue.use(Submenu);
Vue.use(Card);
Vue.use(Row);
Vue.use(Col);
Vue.use(Popover);
Vue.use(MenuItemGroup);
Vue.use(Tabs);
Vue.use(TabPane);
Vue.use(MyPlugin);
Vue.use(Input);
Vue.use(Tooltip);
Vue.use(Loading.directive);
Vue.use(VueI18n);

Vue.prototype.$loading = Loading.service;
Vue.config.productionTip = false

const i18n = new VueI18n({
  locale: 'zh',
  messages: {
    'zh': require('./i18n/langs/zh'),
    'en': require('./i18n/langs/en')
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  components: { App },
  template: '<App/>'
})
