import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'

Vue.use(Router)

export default new Router({
  mode:"history",
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/service',
      name: 'service',
      component:  () => import('../components/service')
    },
    {
      path: '/answer',
      name: 'answer',
      component:  () => import('../components/answer')
    },
    // {
    //   path: '/download',
    //   name: 'download',
    //   component:  () => import('../components/download')
    // },
    // {
    //   path: '/downloadIOS',
    //   name: 'downloadIOS',
    //   component:  () => import('../components/componentsSmall/download_IOS')
    // },
  ]
})
